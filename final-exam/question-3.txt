FINAL: QUESTION 3

In this problem you will update a document in the Enron dataset to illustrate
your mastery of updating documents from the shell. 

Please add the email address "mrpotatohead@mongodb.com" to the list of addresses
in the "headers.To" array for the document with "headers.Message-ID" of
"<8147308.1075851042335.JavaMail.evans@thyme>" 

After you have completed that task, please download final3.zip from the Download
Handout link and run final3-validate.js to get the validation code and put it in
the box below without any extra spaces. The validation script assumes that it is
connecting to a simple mongo instance on the standard port on localhost.

TO FIND THE DOCUMENT THAT NEEDS TO BE MODIFIED:

    use enron
    db.messages.find({
        "headers.Message-ID" : "<8147308.1075851042335.JavaMail.evans@thyme>"
    }).pretty()

TO MODIFIY THE DOCUMENT:

    db.messages.update({
        "headers.Message-ID" : "<8147308.1075851042335.JavaMail.evans@thyme>"
    }, {
        $push : {"headers.To" : "mrpotatohead@mongodb.com"}
    })

GOT THE VALIDATION CODE:

    $ node final3-validate.js 
    Welcome to the Final Exam Q3 Checker. My job is to make sure you correctly updated the document
    Final Exam Q3 Validated successfully!
    Your validation code is: vOnRg05kwcqyEFSve96R


