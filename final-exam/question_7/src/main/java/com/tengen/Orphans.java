package com.tengen;

import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Import each data-set using these commands:
 * <code>
 *     mongoimport --db m101 --collection albums <./albums.json
 *     mongoimport --db m101 --collection images <./images.json
 * </code>
 *
 * To run this code from the command line:
 * <code>
 *      mvn clean compile exec:java -Dexec.mainClass=com.tengen.Orphans
 * </code>
 *
 * To find the total number of images with the tag "sunrises":
 * <code>
 *      > use m101
 *      > db.images.find({"tags" : "kittens"}).size()
 *      44822
 *      > db.images.find({"tags" : "kittens"}).count()
 *      44822
 * </code>
 */
public class Orphans {

    public static void main(final String[] args) throws UnknownHostException {

        MongoClient client = null;

        try {
            client = new MongoClient();
            final DB db = client.getDB("m101");

            final DBCollection albumCollection = db.getCollection("albums");
            final DBCollection imageCollection = db.getCollection("images");

            final Set<Integer> nonOrphanedImageIds = getUniqueImageIds(albumCollection);
            final Set<Integer> allImageIds = getAllImageIds(imageCollection);

            for (final Integer imageId : allImageIds) {

                if (!nonOrphanedImageIds.contains(imageId)) {

                    System.out.println("Removing image id: " + imageId);

                    final BasicDBObject obj = new BasicDBObject();
                    obj.append("_id", imageId);
                    imageCollection.remove(obj);
                }
            }

//            System.out.println("nonOrphanedImageIds.size() = " + nonOrphanedImageIds.size());
//            System.out.println("allImageIds.size() = " + allImageIds.size());

        } finally {
            if (client != null) {
                client.close();
            }
        }

    }

    private static Set<Integer> getUniqueImageIds(final DBCollection albumCollection) {

        final Set<Integer> uniqueImageIds = new HashSet<>();

        try (final DBCursor albumCursor = albumCollection.find()) {

            for (final DBObject album : albumCursor) {

                @SuppressWarnings({"unchecked"})
                final List<Integer> imageIds = (List) album.get("images");
                uniqueImageIds.addAll(imageIds);

            }
        }

        return uniqueImageIds;
    }

    private static Set<Integer> getAllImageIds(final DBCollection imageCollection) {

        final Set<Integer> allImageIds = new HashSet<>();

        try (final DBCursor imageCursor = imageCollection.find()) {

            for (final DBObject image : imageCursor) {
                final Integer id = (Integer) image.get("_id");
                allImageIds.add(id);
            }
        }

        return allImageIds;
    }
}
