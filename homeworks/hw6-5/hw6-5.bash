#!/bin/bash

################################################################################
# This script will be used to create and initialize everything for the 
# assignment.
################################################################################


function cleanup() {
    echo '========================================'
    echo 'Killing all node, mongod and mongos processes.'
    sudo killall node
    sudo killall mongod
    sudo killall monogs

    echo '========================================'
    echo 'Removing data files.'
    rm -rf /data/rs1
    rm -rf /data/rs2
    rm -rf /data/rs3
    sudo rm -rfv /data

    echo '========================================'
    echo 'Removing node_modules.'
    rm -rf node_modules

    echo '========================================'
    echo 'Removing log and dated log files.'
    rm -rfv 1.log*
    rm -rfv 2.log*
    rm -rfv 3.log*
}

cleanup

echo '========================================'
echo 'Creating three directories for the three mongod processes.'
sudo mkdir -p /data
sudo chown michael:michael /data
mkdir -pv /data/rs1
mkdir -pv /data/rs2
mkdir -pv /data/rs3

echo '========================================'
echo 'Starting three mongo instances.'
mongod --replSet m101 --logpath "1.log" --dbpath /data/rs1 --port 27017 --smallfiles --oplogSize 64 --fork
mongod --replSet m101 --logpath "2.log" --dbpath /data/rs2 --port 27018 --smallfiles --oplogSize 64 --fork
mongod --replSet m101 --logpath "3.log" --dbpath /data/rs3 --port 27019 --smallfiles --oplogSize 64 --fork

echo '========================================'
echo 'Connecting to a mongo shell to make sure it comes up.'
mongo --port 27017 << 'EOF'
config = { _id: "m101", members:[
          { _id : 0, host : "localhost:27017"},
          { _id : 1, host : "localhost:27018"},
          { _id : 2, host : "localhost:27019"} ]
};
rs.initiate(config);
rs.status()
EOF

echo '========================================'
echo 'Running the validation scripts on the assignment:'
sleep 20
npm install
node validate.js

echo '########################################'
echo 'Copy the validation code above into the assignment at:  university.mongodb.com'
echo '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'

cleanup

