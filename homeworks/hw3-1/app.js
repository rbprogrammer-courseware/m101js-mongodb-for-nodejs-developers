var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/school', function(err, db) {
    if(err) throw err;

    db.collection('students').find({}).toArray(function(err, docs) {
        if(err) throw err;

        // Ensure there is actually a document returned.
        if(!docs) {
            console.log('No documents found!');
            return db.close();
        }

        // For each document...
        docs.forEach(function (doc) {

            // Find the lowest homework grade.
            var lowest_score = 100;
            var lowest_score_index = -1;
            for (var i=0; i<doc.scores.length; i++) {
                if (doc.scores[i].type == "homework") {
                    var current_score = doc.scores[i].score;
                    if (current_score < lowest_score) {
                        lowest_score = current_score;
                        lowest_score_index = i;
                    }
                }
            }

            // Blasted error checking.
            if (lowest_score_index < 0) {
                throw Error("Invalid index: " + lowest_score_index + " for document: " + doc)
            }

            // Remove the lowest homework.
            doc.scores.splice(lowest_score_index, 1);

            // Update the document.
            var query = {
                _id : doc['_id']
            };

            db.collection('students').update(query, doc, function(err, updated) {
                if(err) throw err;
                console.dir('Successfully updated document: ' + doc.name);
            });
        });

        // I have no fucking idea why I have to comment this out.  However when
        // I do this, the command "node app.js" will just hang.  I have to 
        // CTRL+C that bitch.  If I were to uncomment this, I get a stack trace
        // error on the console.  Urg!
        //return db.close();

    });
});

